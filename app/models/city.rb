require 'csv'
class City < ActiveRecord::Base
  belongs_to :country
  attr_accessible :lat, :lng, :name, :state, :zip

  def self.city_list
   csv_text = File.read("#{Rails.public_path}/uszipsv1.1.csv")
   csv = CSV.parse(csv_text, :headers => true)
   csv.each do |row|
    country = Country.find_by_id(5)
    country.cities.find_or_create_by(name: row['city'],zip: row['zip'],lat: row['lat'],lng: row['lng'],state: row['state'])
   end
  end

end
